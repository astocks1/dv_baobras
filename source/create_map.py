import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import geopandas as gpd
from shapely.geometry import Point
from pyproj import Proj
import os
import imageio
import numpy as np
import matplotlib.patches as mpatches
import matplotlib.cm as cm
import csv

obras = pd.read_csv("C:\\Users\\UA23501\\OneDrive - The Dow Chemical Company\\Documents\\Personal\\MiM\\Visualizacion de datos\\TP\\dv_baobras\\data\\observatorio-de-obras-urbanas.csv")
fp = "C:\\Users\\UA23501\\OneDrive - The Dow Chemical Company\\Documents\\Personal\\MiM\\Visualizacion de datos\\TP\\dv_baobras\\data\\barrios-ciudad-autnoma-de-buenos-aires-shp.shp"

#fp = "C:\\Users\\UA23501\\OneDrive - The Dow Chemical Company\\Documents\\Personal\\MiM\\Visualizacion de datos\\TP\\dv_baobras\\data\\vazquez-brust-regin-metropolitana-de-buenos-aires\\RMBA.shp"
ba_map = gpd.read_file(fp)

#convert to dates
obras['fecha_inicio'] = pd.to_datetime(obras['fecha_inicio'])
obras['fecha_fin_inicial'] = pd.to_datetime(obras['fecha_fin_inicial'])

#convert to numbers
obras['lng'] = round(obras['lng'],5).astype(float)
obras['lat'] = round(obras['lat'],5).astype(float)
obras['monto_contrato'] = pd.to_numeric(obras['monto_contrato'], errors='coerce')

#create coordenadas
obras['coordenadas']= list(zip(obras.lng, obras.lat))
obras['coordenadas'] = obras['coordenadas'].apply(Point)

#get date range
date_min = obras['fecha_inicio'].min()
date_max = obras['fecha_inicio'].max()
date_range = pd.date_range(start=date_min, end=date_max, freq='MS')[::1]

#map params
output_path = 'C:\\Users\\UA23501\\OneDrive - The Dow Chemical Company\\Documents\\Personal\\MiM\\Visualizacion de datos\\TP\\dv_baobras\\data\\maps'
markersize_max = 1100
markersize_min = 200
markersize_range = markersize_max - markersize_min
max_contract = obras['monto_contrato'].max()
min_contract = obras['monto_contrato'].min()
contract_range = max_contract - min_contract

#iterate through each month to find active works during month
i=0
for month in date_range:
    #plot map
    ax = ba_map.plot(color='#130b0b', figsize=(30,20), edgecolor='white')
    ax.set_axis_off()

    #produce one geodataframe per period
    gdf =  gpd.GeoDataFrame(obras[['id', 'fecha_inicio', 'fecha_fin_inicial', 'coordenadas', 'monto_contrato', 'comuna', 'barrio', 'direccion','porcentaje_avance']].loc[(obras['fecha_inicio'] < month) & (obras['fecha_fin_inicial'] > month)], geometry='coordenadas')

    #percentage_complete variable based on porcentaje_avance
    #gdf['percentage_complete'] = np.where(gdf['porcentaje_avance'] == 100, (((month - gdf['fecha_inicio'])/np.timedelta64(1,'D'))/((gdf['fecha_fin_inicial'] - gdf['fecha_inicio'])/np.timedelta64(1,'D'))), gdf['porcentaje_avance']/100)
    gdf['month'] = month
    gdf['percentage_complete'] = (((gdf['month'] - gdf['fecha_inicio'])/np.timedelta64(1,'D'))/((gdf['fecha_fin_inicial'] - gdf['fecha_inicio'])/np.timedelta64(1,'D')))

    #plot works over map, normalized markersize by contract ammt
    gdf.plot(ax=ax, markersize=(((gdf['monto_contrato']-min_contract)/contract_range)*markersize_range)+markersize_min, cmap='cool', column=gdf['percentage_complete'], legend=False)

    #add title
    ax.set_title('Obras en construccion al ' + str(month.date()), fontsize=45)

    cmap = cm.get_cmap('cool')
    patch_05 = mpatches.Patch(color=cmap(0.0), label='0-5% completo')
    patch_10 = mpatches.Patch(color=cmap(0.1), label='5-10% completo')
    patch_15 = mpatches.Patch(color=cmap(0.15), label='10-15% completo')
    patch_20 = mpatches.Patch(color=cmap(0.2), label='15-20% completo')
    patch_25 = mpatches.Patch(color=cmap(0.25), label='20-25% completo')
    patch_30 = mpatches.Patch(color=cmap(0.30), label='25-30% completo')
    patch_35 = mpatches.Patch(color=cmap(0.35), label='30-35% completo')
    patch_40 = mpatches.Patch(color=cmap(0.4), label='35-40% completo')
    patch_45 = mpatches.Patch(color=cmap(0.45), label='40-45% completo')
    patch_50 = mpatches.Patch(color=cmap(0.5), label='45-50% completo')
    patch_55 = mpatches.Patch(color=cmap(0.55), label='50-55% completo')
    patch_60 = mpatches.Patch(color=cmap(0.6), label='55-60% completo')
    patch_65 = mpatches.Patch(color=cmap(0.65), label='60-65% completo')
    patch_70 = mpatches.Patch(color=cmap(0.7), label='65-70% completo')
    patch_75 = mpatches.Patch(color=cmap(0.75), label='70-75% completo')
    patch_80 = mpatches.Patch(color=cmap(0.80), label='75-80% completo')
    patch_85 = mpatches.Patch(color=cmap(0.85), label='80-85% completo')
    patch_90 = mpatches.Patch(color=cmap(0.90), label='85-90% completo')
    patch_95 = mpatches.Patch(color=cmap(0.95), label='90-95% completo')

    ax.legend(handles=[patch_10, patch_20, patch_30, patch_40, patch_50, patch_60, patch_70,  patch_80, patch_90])

    #save image
    filepath = os.path.join(output_path, str(i)+'.jpg')
    plt.savefig(filepath)
    i = i + 1

    #save csv
    with open("C:\\Users\\UA23501\\OneDrive - The Dow Chemical Company\\Documents\\Personal\\MiM\\Visualizacion de datos\\TP\\dv_baobras\\data\\maps\\report.csv", 'a') as f:
        gdf.to_csv(f, header=False)
        month
