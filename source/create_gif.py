import os
import imageio
from natsort import natsorted

##Convert jpg's to gif
png_dir = 'C:\\Users\\UA23501\\OneDrive - The Dow Chemical Company\\Documents\\Personal\\MiM\\Visualizacion de datos\\TP\\dv_baobras\\data\\maps'
list = natsorted(os.listdir(png_dir))
images = []
for file_name in list:
    if file_name.endswith('.jpg'):
        file_path = os.path.join(png_dir, file_name)
        images.append(imageio.imread(file_path))
imageio.mimsave('C:\\Users\\UA23501\\OneDrive - The Dow Chemical Company\\Documents\\Personal\\MiM\\Visualizacion de datos\\TP\\dv_baobras\\data\\maps\\obras.gif', images)
