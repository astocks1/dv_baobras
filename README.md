# Buenos Aires Public Works Data Visualization

Work done for Data Visualization course - Master in Management and Analytics at Universidad Torcuato Di Tella

Work includes
  - Geopandas mapping
  - Holoviews for choropleth maps
 
